import asyncio

import disnake
import youtube_dl  # type: ignore
from disnake.ext import commands

from utils import Embeds

# Suppress noise about console usage from errors
youtube_dl.utils.bug_reports_message = lambda: ""

ytdl_format_options = {
    "format": "bestaudio/best",
    "outtmpl": "%(extractor)s-%(id)s-%(title)s.%(ext)s",
    "restrictfilenames": True,
    "noplaylist": False,
    "nocheckcertificate": True,
    "ignoreerrors": False,
    "logtostderr": False,
    "quiet": True,
    "no_warnings": True,
    "default_search": "auto",
    "source_address": "0.0.0.0",  # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {"options": "-vn"}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)


class YTDLSource(disnake.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get("title")
        self.url = data.get("url")

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(
            None, lambda: ytdl.extract_info(url, download=not stream)
        )
        assert data

        if "entries" in data:
            # take first item from a playlist
            data = data["entries"][0]

        filename = data["url"] if stream else ytdl.prepare_filename(data)
        assert filename

        return cls(disnake.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)


class Music(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="join", description="Joins a voice channel")
    async def join(self, interaction, *, channel: disnake.VoiceChannel):
        await self.slash_join(interaction, channel)

    @commands.slash_command(name="join")
    async def slash_join(self, interaction, channel: disnake.VoiceChannel):
        """
        Joins a voice channel

        Parameters
        ----------
        channel: The voice channel to join
        """
        await interaction.send(
            embed=Embeds.emb(
                Embeds.green, "Voice Channel Joined", f"Channel Name: {channel}"
            )
        )
        if interaction.guild.voice_client is not None:
            return await interaction.guild.voice_client.move_to(channel)

        await channel.connect()

    @commands.command(name="play", description="Plays music from youtube")
    async def play(self, interaction, search):
        await self.slash_play(interaction, search)

    @commands.slash_command(name="play", description="Plays music from youtube")
    async def slash_play(self, interaction, search):
        """
        Plays music from youtube

        Parameters
        ----------
        search: The search query to search for
        """
        url = search
        try:
            await interaction.response.defer()
        except Exception:
            pass
        await self.ensure_voice(interaction)
        player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
        interaction.guild.voice_client.play(
            player,
            after=lambda e: interaction.send(
                embed=Embeds.emb(Embeds.red, "Player Error", ephemeral=True),
            )
            if e
            else None,
        )
        await interaction.send(
            embed=Embeds.emb(Embeds.blue, "Playing...", f"Name: {player.title}"),
            ephemeral=True,
        )

    @commands.command(name="volume", description="Changes the player's volume")
    async def volume(self, interaction, volume: int):
        await self.slash_volume(interaction, volume)

    @commands.slash_command(name="volume")
    async def slash_volume(self, interaction, volume: int):
        """
        Changes the player's volume

        Parameters
        ----------
        volume: The volume to change to
        """
        if interaction.guild.voice_client is None:
            return await interaction.send(
                embed=Embeds.emb(Embeds.red, "Not connected to a voice channel.")
            )

        interaction.guild.voice_client.source.volume = volume / 100
        await interaction.send(
            embed=Embeds.emb(Embeds.blue, "Volume", f"Changed volume to {volume}%")
        )

    @commands.command(
        name="stop", description="Stops and disconnects the bot from voice"
    )
    async def stop(self, interaction):
        await self.slash_stop(interaction)

    @commands.slash_command(name="stop")
    async def slash_stop(self, interaction):
        """Stops and disconnects the bot from voice"""
        await interaction.send(
            embed=Embeds.emb(Embeds.red, "Voice Channel Disconnected")
        )
        await interaction.guild.voice_client.disconnect()

    async def ensure_voice(self, interaction):
        if interaction.guild.voice_client is None:
            if interaction.author.voice:
                await interaction.author.voice.channel.connect()
            else:
                await interaction.send(
                    embed=Embeds.emb(
                        Embeds.red,
                        "Your aren't connected to voice Channel",
                        "Connect to voice channel",
                    )
                )
        elif interaction.guild.voice_client.is_playing():
            interaction.guild.voice_client.stop()


def setup(client: commands.Bot):
    client.add_cog(Music(client))
