# TODO: Add custom block list , protocol list

import re

from disnake.ext import commands

from bot import client
from utils import Embeds, db


class Anti_abusive(commands.Cog):
    def __init__(self, client):
        self.bot = client

    @commands.Cog.listener()
    async def on_message(self, message):
        # print(message)
        # print(client.user.mention))
        # if client.user.mention:
        #    await message.channel.send(embed=Embeds.emb(Embeds.red,"Server Prefix",prefixes[str(message.guild.id)]["prefix"]))
        try:
            with open(f"Logs/{message.guild.name}.log", "a") as msg:
                msg.write(
                    f"{message.channel.name} {message.channel.id}--> {message.author}: {message.content}\n"
                )
        except:
            pass

        msg_content = message.content.lower()

        curseWord = [
            "slut",
            "f*ck",
            "fuck",
            "pussy",
            "bitch",
            "ass",
            "milf",
            "loudfatty",
            "buttshaker",
            "boob",
            "penis",
            "vagina",
            "shit",
            "dick",
            "porn",
            "squirt",
            "shemale",
            "sperm",
            "bsdm",
            "sexylady",
            "swollen ovary",
            "sexy",
            "cunt",
            "motherfliper",
        ]
        protocol = ["http://", "https://", "://"]
        mention = ["@here", "@everyone"]

        result = db.config.find_one({"guild_id": message.guild.id})
        if result is not None:
            try:
                if result["Link Blocker"] == "deactivate":
                    protocol = []
            except KeyError:
                ...
            try:
                if result["Anti-Abusive"] == "deactivate":
                    curseWord = []
            except KeyError:
                ...
            try:
                if result["@everyone/@here mention blocker"] == "deactivate":
                    mention = []
            except KeyError:
                ...

        if str(message.content) == f"<@{client.user.id}>":
            result = db.config.find_one({"guild_id": message.guild.id,
                                         "prefix": {"$exists":True}
                                         })
            if result is not None:
                prefix = result["prefix"]
            else:
                prefix = "!!"
            await message.channel.send(
                embed = Embeds.emb(
                    Embeds.red,
                    f"Use `/help` or `{prefix}commands`",
                )
            )
        try:
            if message.author.id == self.bot.user.id:
                return
            elif not "name='Protocol_access'" in str(message.author.roles) and message.author.roles:
                if any(word in msg_content for word in protocol):
                    await message.delete()
                elif any(words in msg_content for words in mention):
                    await message.delete()
                if not message.channel.is_nsfw():
                    terms = [re.search(rf"\b{i}", msg_content) for i in curseWord]
                    for j in terms:
                        if j != None:
                            await message.delete()
                            break

        except Exception:
            pass

def setup(client: commands.Bot):
    client.add_cog(Anti_abusive(client))
