import disnake
from disnake.ext import commands

from utils import Embeds, db


def fetch_entry(param) -> str:
    args = "[+]"
    if isinstance(param, list):
        for i in param:
            args = args + f"\n[+] `{i}`"
    else:
        args = f"[+] `{str(param)}`"
    return args


class Command_error_handling(commands.Cog):
    def __init__(self, client):
        self.bot = client

    @commands.Cog.listener()
    async def on_slash_command_error(self, ctx, error):

        if isinstance(error, commands.errors.CommandInvokeError):
            if isinstance(error.original, disnake.Forbidden):
                await ctx.send(
                    embed=Embeds.emb(
                        Embeds.yellow, "Immune User",
                        "The User on whom you are trying to is immune to "
                        "this commands"
                    ),
                    delete_after=10,
                )

        elif isinstance(error, commands.CheckAnyFailure):
            if isinstance(error.errors[1], commands.errors.MissingPermissions):
                checks = error.errors[1].missing_permissions
                parsed_str = "\n\n[-] -> " + "\n\n[-] -> ".join(checks)
                embed = Embeds.emb(
                    Embeds.red,
                    "You are missing following permissions "
                    "to use this command!",
                    parsed_str,
                )
                await ctx.send(embed=embed, delete_after=10)

        elif isinstance(error, commands.errors.CommandNotFound):
            result = db.config.find_one({"guild_id": ctx.author.guild.id,
                                         "prefix": {"$exists": True}})
            if result is not None:
                prefix = result["prefix"]
            else:
                prefix = "!!"
            await ctx.send(
                embed=Embeds.emb(
                    Embeds.red,
                    "No Such Command is available! "
                    f"Use  `/help` or `{prefix}commands`  for command list!",
                ),
                delete_after=10,
            )

        elif isinstance(error, commands.errors.NSFWChannelRequired):
            await ctx.send(
                embed=Embeds.emb(
                    Embeds.red,
                    "NSFW Channel Required",
                    "This command is only for nsfw channels only",
                ),
                delete_after=10,
            )

        elif isinstance(error, commands.MissingRequiredArgument):
            args = fetch_entry(error.param)
            await ctx.send(
                embed=Embeds.emb(Embeds.red, "Arguments Are Missing!", args),
                delete_after=10,
            )

        elif isinstance(error, AttributeError):
            await ctx.send(
                embed=Embeds.emb(Embeds.red, "ERROR", str(error)),
                delete_after=10
            )

        elif isinstance(error, commands.BadArgument):
            await ctx.send(
                embed=Embeds.emb(Embeds.red,
                                 "Argument Error", "Pass valid argument"),
                delete_after=10,
            )

        elif isinstance(error, commands.NotOwner):
            await ctx.send(
                embed=Embeds.emb(
                    Embeds.red,
                    "Owner Command Only",
                    "This command is only for the owner of the bot",
                ),
                delete_after=10,
            )

        elif "This command cannot be used in private messages." in str(error):
            await ctx.send(
                embed=Embeds.emb(Embeds.red,
                                 "Command not available in private"),
                delete_after=10,
            )

        else:
            with open("Logs/error.log", "a") as file:
                file.write("\n\n")
                file.write("-" * 10)
                file.write(f"Error: {str(error)}")
                await ctx.send(
                    embed=Embeds.emb(
                        Embeds.red,
                        "Oops! Something went wrong!",
                        f"Type of error: {type(error)}\n"
                        f"Error: {str(error)}",
                    ),
                    delete_after=10,
                )
                raise error

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        await self.on_slash_command_error(ctx, error)


def setup(client: commands.Bot):
    client.add_cog(Command_error_handling(client))
