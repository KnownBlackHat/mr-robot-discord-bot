import datetime

import disnake
from disnake.ext import commands

from utils import Embeds, db, features

MISSING = "MISSING"


class Moderation(commands.Cog):
    def __init__(self, client):
        self.bot = client

    # add and remove feature
    @commands.slash_command(name="manage_features")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_guild=True)
    )
    async def manage_features(
        self,
        interaction,
        option: str = commands.Param(choices=["Activate", "Deactivate"]),
        feature: str = commands.Param(
            choices=features
        ),
    ):
        """
        Toggles my features in server

        Parameters
        ----------
        option : Activate or Deactivate
        feature : Feature to toggle
        """
        if option == "Activate":
            data = {"$set": {
                feature: "activate",
                }}
            db.config.update_one({"guild_id": interaction.guild.id}, data,
                                 upsert=True)
            await interaction.send(embed=Embeds.emb(Embeds.green, "Activated",
                                                    feature))
        elif option == "Deactivate":
            data = {"$set": {
                feature: "deactivate",
                }}
            db.config.update_one({"guild_id": interaction.guild.id},
                                 data, upsert=True)
            await interaction.send(embed=Embeds.emb(Embeds.red, "Deactivated",
                                                    feature))

    # clear

    @commands.command(name="clear", description="Deletes the messages")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_messages=True)
    )
    async def clear(self, interaction, amount=2):
        await self.slash_clear(interaction, amount)

    @commands.slash_command(name="clear")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_messages=True)
    )
    async def slash_clear(self, interaction, amount=1):
        """
        Deletes the messages

        Parameters
        ----------
        amount : Amount of messages to delete
        """
        await interaction.channel.purge(limit=int(amount))
        await interaction.send(
            embed=Embeds.emb(Embeds.yellow, f"{amount} message deleted"),
            delete_after=5)

    @commands.command(name="change_prefix",
                      description="Changes Server Prefix")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_guild=True)
    )
    async def changeprefix(self, interaction, *, prefix):
        await self.slash_changeprefix(interaction, prefix)

    @commands.slash_command(name="change_prefix")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_guild=True)
    )
    async def slash_changeprefix(self, interaction, prefix):
        """
        Changes the prefix of the server

        Parameters
        ----------
        prefix : New prefix of the server
        """
        if prefix.startswith('"') and prefix.endswith('"'):
            prefix = prefix.replace('"', "")
        elif prefix.startswith("'") and prefix.endswith("'"):
            prefix = prefix.replace("'", "")
        db.config.update_one(
                {"guild_id": interaction.guild.id},
                {"$set": {
                    "guild_id": interaction.guild.id,
                    "guild_name": interaction.guild.name,
                    "prefix": prefix,
                    }},
                upsert=True,
                )
        await interaction.send(
            embed=Embeds.emb(Embeds.green, "New Server Prefix", f"`{prefix}`")
        )

    # Role Management

    @commands.command(name="addrole", description="Adds the roles")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_roles=True)
    )
    async def addrole(self, interaction, user: disnake.Member,
                      role: disnake.Role):
        await self.slash_addrole(interaction, user, role)

    @commands.slash_command(name="addrole")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_roles=True)
    )
    async def slash_addrole(
        self, interaction, user: disnake.Member, role: disnake.Role
    ):
        """
        Assign the roles

        Parameters
        ----------
        user : User to add role
        role : Role to add
        """
        role = disnake.utils.get(user.guild.roles, name=str(role))
        await user.add_roles(role)
        await interaction.send(
            embed=Embeds.emb(
                Embeds.green,
                "Role Assigned",
                f"{user.mention} Has Got  `{role}`  Role !",
            ),
            delete_after=10,
        )
        try:
            await user.send(
                embed=Embeds.emb(
                    Embeds.green,
                    "Role Assigned",
                    f" You got `{role}` Role In {interaction.guild.name} !",
                )
            )
        except Exception:
            ...

    @commands.command(name="rmrole", description="Removes the roles")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_roles=True)
    )
    async def rmrole(self, interaction, user: disnake.Member,
                     role: disnake.Role):
        await self.slash_rmrole(interaction, user, role)

    @commands.slash_command(name="rmrole")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_roles=True)
    )
    async def slash_rmrole(self, interaction, user: disnake.Member,
                           role: disnake.Role):
        """
        Removes the roles

        Parameters
        ----------
        user : User to remove role
        role : Role to remove
        """
        role = disnake.utils.get(user.guild.roles, name=str(role))
        await user.remove_roles(role)
        await interaction.send(
            embed=Embeds.emb(
                Embeds.red,
                "Role Removed",
                f"{user.mention} Was Removed  `{role}` Role !",
            ),
            delete_after=10,
        )
        try:
            await user.send(
                embed=Embeds.emb(
                    Embeds.red,
                    "Role Removed",
                    "You got removed from "
                    f"`{role}` Role In {interaction.guild.name}!",
                )
            )
        except disnake.Forbidden:
            ...

    @commands.command(name="unban", description="Unbans the member")
    @commands.check_any(commands.is_owner(),
                        commands.has_permissions(ban_members=True))
    async def unban(self, interaction, member):
        await self.self_unban(interaction, member)

    @commands.slash_command(name="unban")
    @commands.check_any(commands.is_owner(),
                        commands.has_permissions(ban_members=True))
    async def self_unban(self, interaction, member):
        """
        Unbans the member

        Parameters
        ----------
        member : Member to unban
        """
        banned_users = await interaction.guild.bans()
        member_name, member_discriminator = member.split("#")
        for ban_entry in banned_users:
            user = ban_entry.user
            if (user.name, user.discriminator) == (member_name,
                                                   member_discriminator):
                await interaction.guild.unban(user)
                await interaction.send(
                    embed=Embeds.emb(Embeds.green,
                                     "Unbanned", f"Unbanned: {user}"),
                    delete_after=10,
                )
                try:
                    await member.send(
                        embed=Embeds.emb(name="You Were Unbanned From "
                                         f"{interaction.guild.name} Server!"))
                except disnake.Forbidden:
                    pass

                return

    @commands.command(name="ban", description="Bans the member")
    @commands.check_any(commands.is_owner(),
                        commands.has_permissions(ban_members=True))
    async def ban(self, interaction, member: disnake.Member, *, reason=None):
        await self.slash_ban(interaction, member, reason)

    @commands.slash_command(name="ban")
    @commands.check_any(commands.is_owner(),
                        commands.has_permissions(ban_members=True))
    async def slash_ban(self, interaction,
                        member: disnake.Member, reason=None):
        """
        Bans the member

        Parameters
        ----------
        member : Member to ban
        reason : Reason for ban
        """
        await member.ban(reason=reason)
        try:
            await member.send(
                embed=Embeds.emb(
                    Embeds.red,
                    "You Were Banned From "
                    f"{interaction.guild.name} Server!",
                    f"Reason: {reason}",
                )
            )
        except disnake.Forbidden:
            pass
        await interaction.send(
            embed=Embeds.emb(Embeds.red, "Banned",
                             f"Banned: {member} Reason: {reason}")
        )

    @commands.command(name="timeout",
                      description="Temporarily mutes the member")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(moderate_members=True)
    )
    async def edit(
        self,
        interaction,
        member: disnake.Member,
        hours: int = 1,
        days: int = 0,
        reason: str = None,
    ):
        await self.slash_edit(interaction, member, hours, days, reason)

    @commands.slash_command(name="timeout")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(moderate_members=True)
    )
    async def slash_edit(
        self,
        interaction,
        member: disnake.Member,
        hours: int = 1,
        days: int = 0,
        reason: str = None,
    ):
        """
        Temporarily mutes the member

        Parameters
        ----------
        member : Member To Mute
        hours : Hours To Mute
        days : Days To Mute
        reason : Reason For Mute
        """
        await interaction.response.defer()
        if days == 0 and hours == 0:
            await interaction.send(
                embed=Embeds.emb(
                    Embeds.red, "Error", "User can't be muted for 0 minutes"),
                delete_after=10,
            )
        else:
            await member.edit(
                timeout=datetime.timedelta(days=days,
                                           hours=hours).total_seconds()
            )
            try:
                await member.send(
                    embed=Embeds.emb(
                        Embeds.red,
                        "You are Temporarily Muted "
                        f"in the {interaction.guild.name} server",
                        f"Reason: {reason}",
                    )
                )
            except disnake.Forbidden:
                pass
            await interaction.send(
                embed=Embeds.emb(
                    Embeds.red,
                    "Temporarily Muted",
                    f"{member.mention} is muted "
                    f"for {datetime.timedelta(days=days,hours=hours)}",
                ),
                delete_after=10,
            )

    @commands.command(name="kick", description="Kicks the member")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(kick_members=True)
    )
    async def kick(self, interaction, member: disnake.Member, *, reason=None):
        await self.slash_kick(interaction, member, reason)

    @commands.slash_command(name="kick", description="Kicks the member")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(kick_members=True)
    )
    async def slash_kick(self, interaction,
                         member: disnake.Member, reason=None):
        """
        Kicks the member

        Parameters
        ----------
        member : Member to kick
        reason : Reason for kick
        """
        await member.kick(reason=reason)
        await interaction.response.defer()
        try:
            await member.send(
                embed=Embeds.emb(
                    Embeds.red,
                    f"You Were Kicked From {interaction.guild.name} Server!",
                    f"Reason: {reason}",
                )
            )
        except Exception:
            ...
        await interaction.send(
            embed=Embeds.emb(Embeds.red, "Kicked",
                             f"Kicked: {member} Reason: {reason}")
        )

    @commands.command(name="dm", description="Dm's the user")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(moderate_members=True)
    )
    async def dm_custom(
        self, interaction, member: disnake.Member, title: str, msg: str
    ):
        await self.slash_dm_custom(interaction, member, title, msg)

    @commands.slash_command(name="dm")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(moderate_members=True)
    )
    async def slash_dm_custom(
        self, interaction, member: disnake.Member, title: str, msg: str
    ):
        """
        Dm's the user

        Parameters
        ----------
        member : Member To Dm
        title : Title Of Dm
        msg : Message To Send
        """
        try:
            interaction.response.defer(ephemeral=True)
        except AttributeError:
            ...
        try:
            await member.send(embed=Embeds.emb(Embeds.yellow, title, msg))
            await interaction.send(
                embed=Embeds.emb(Embeds.yellow, title, msg), delete_after=10
            )
        except disnake.Forbidden:
            await interaction.send(
                embed=Embeds.emb(Embeds.red, "Dm not sent"), delete_after=10
            )

    @commands.command(name="warn", description="Warns the user")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(moderate_members=True)
    )
    async def warn(self, interaction, member: disnake.Member, msg: str):
        await self.slash_warn(interaction, member, msg)

    @commands.slash_command(name="warn", description="Warns the user")
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(moderate_members=True)
    )
    async def slash_warn(self, interaction, member: disnake.Member, msg: str):
        """
        Warns the user

        Parameters
        ----------
        member : Member To Warn
        msg : Message To Send
        """
        await interaction.send(
            embed=Embeds.emb(
                Embeds.red,
                f"WARNING {member}", f"{member.mention} --> {msg}"),
            delete_after=10,
        )
        try:
            await member.send(embed=Embeds.emb(Embeds.red, "WARNING", msg))
        except Exception as error:
            print(error)


def setup(client: commands.Bot):
    client.add_cog(Moderation(client))
