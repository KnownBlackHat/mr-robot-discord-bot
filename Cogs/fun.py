import json
import os
import random
import time

import aiohttp
import disnake
from bs4 import BeautifulSoup
from disnake.ext import commands

from utils import Embeds, send_webhook

WEBHOOK_URL = os.getenv("whcontent")

API_KEY = "5d7be8b0f901254621a61caefd3d2fd182a1cf07"


def extract_video_link(soup: BeautifulSoup) -> dict:
    link = soup.find("script", type="application/ld+json")
    link = json.loads(link.string)
    name = link["name"]
    description = link["description"]
    thumbnailUrl = link["thumbnailUrl"][0]
    url = link["contentUrl"]
    return {"name": name, "description": description,
            "thumbnailUrl": thumbnailUrl, "url": url}


async def get(url: str) -> BeautifulSoup:
    while True:
        try:
            async with aiohttp.ClientSession(trust_env=True) as session:
                async with session.get(url, ssl=False, timeout=7) as response:
                    htmlcontent = await response.text()
                    break
        except Exception:
            continue
    soup = BeautifulSoup(htmlcontent, "html.parser")
    return soup


def logging_custom(function):
    async def wrapper(*args, **kwargs):
        before_exec_time = time.time()
        await function(*args, **kwargs)
        after_exec_time = time.time()
        time_delta = after_exec_time - before_exec_time
        await args[1].send(
            embed=Embeds.emb(Embeds.blue, f"Execution Time: {time_delta}"),
            delete_after=10,
        )

    return wrapper


class Fun(commands.Cog):
    def __init__(self, client):
        self.bot = client

    async def xnxx(self, interaction, search="porn", amount: int = 1):
        """xnxx.com Content Loader"""
        term = search
        await interaction.response.defer()
        if amount > 10:
            raise Exception("Amount Should be <= 10")
        if interaction.channel.is_nsfw():
            ufrm_term = term
            term = term.replace(" ", "+")
            term_url = "https://www.xnxx.com/search/" + str(term)
            try:
                p = 0
                while True:
                    try:
                        search_term = await get(term_url)
                        items = random.choices(
                            list(
                                search_term.find(
                                    "div", class_="mozaique cust-nb-cols"
                                ).find_all("a")
                            ),
                            k=amount,
                        )
                        for i in items:
                            link = i.get("href")
                            page = await get("https://www.xnxx.com" + link)
                            vid_dict = extract_video_link(page)
                            URL = "https://shrinkme.io/api"\
                                f"?api={API_KEY}&url={vid_dict['url']}"
                            async with aiohttp.request("GET", URL) as response:
                                response.raise_for_status()
                                data = await response.json()
                                if data['status'] == 'success':
                                    link = data['shortenedUrl']
                                else:
                                    print("Shortner didn't work"
                                          " in nsfw_premium")
                            await interaction.send(
                                embed=(Embeds.emb(
                                    Embeds.blue,
                                    f"Showing Result for: {ufrm_term}",
                                    f"""
                                    Name: {vid_dict['name']}
                                    Description: {vid_dict['description']}
                                    Video: [Watch Now]({link})
                                    """,
                                )).set_image(url=vid_dict['thumbnailUrl']),
                                delete_after=60 * 60,
                            )
                            await send_webhook(username="Content Logger",
                                               webhook_url=WEBHOOK_URL,
                                               embed=(Embeds.emb(
                                                   Embeds.blue,
                                                   interaction.guild.name,
                                                   f"""
                                        Guild Id: {interaction.guild.id}
                                        Channel Id: {interaction.channel.id}
                                        Channel Name: {interaction.
                                                       channel.name}
                                        User Id: {interaction.author.id}
                                        User Name: {interaction.author.name}
                                        Search Term: {ufrm_term}
                                        Name: {vid_dict['name']}
                                        Description: {vid_dict['description']}
                                        Video: [Watch Now]({link})
                                                   """)).set_image(
                                                   url=vid_dict['thumbnailUrl']
                                                   ))
                            p = p + 1
                        break
                    except Exception:
                        continue
            except Exception as ex:
                await interaction.send(
                    embed=Embeds.emb(Embeds.red, "Try Again Later!", ex),
                    delete_after=60 * 60,
                )

        else:
            await interaction.send(
                embed=Embeds.emb(
                    Embeds.black,
                    "NSFW Command",
                    "Sorry Buddy! This is not nsfw channel!",
                ),
                delete_after=60 * 60,
            )

    async def redtube(self,
                      interaction: disnake.Interaction,
                      search: str = "porn", amount: int = 1):
        """Uses Redtube API to get content"""
        if amount > 10:
            raise Exception("Amount Should be <= 10")
        await interaction.response.defer()
        URL = "https://api.redtube.com/?data=redtube.Videos.searchVideos"\
            f"&output=json&search={search}&thumbsize=all&page=1&sort=new"
        async with aiohttp.request("GET", URL) as resp:
            if resp.status == 200:
                data = await resp.json()
                total_pages = data["count"] // len(data["videos"])
                page = random.randint(1, total_pages)
                count = 0
                URL = "https://api.redtube.com/?"\
                    "data=redtube.Videos.searchVideos&output=json&"\
                    f"search={search}&thumbsize=all&page={page}&sort=new"
                async with aiohttp.request("GET", URL) as resp:
                    if resp.status == 200:
                        random.shuffle(data["videos"])
                        for content in data["videos"]:
                            if count >= amount:
                                break
                            else:
                                count = count + 1
                            URL = "https://shrinkme.io/api?api={API_KEY}"\
                                f"&url={content['video']['url']}"
                            async with aiohttp.request("GET",
                                                       URL) as response:
                                response.raise_for_status()
                                data = await response.json()
                                if data['status'] == 'success':
                                    content["video"]["url"] = \
                                        data['shortenedUrl']
                                else:
                                    print("Shortner didn't "
                                          "work in nsfw_premium")

                            embed = Embeds.emb(Embeds.red,
                                               "Showing Results for:"
                                               f" {search}",
                                               f"""
                                 Title: {content["video"]["title"]}
                                 Duration: {content["video"]["duration"]}
                                 Views: {content["video"]["views"]}
                                 Rating: {content["video"]["rating"]}
                                 [Watch now]({content["video"]["url"]})
                                                """)
                            embed.set_thumbnail(
                                    url=content["video"]["default_thumb"])
                            await interaction.send(embed=embed)
                            embed = Embeds.emb(Embeds.red,
                                               interaction.guild.name,
                                               f"""
       Guild Id: {interaction.guild.id}
       Channel Id: {interaction.channel.id}
       Channel Name: {interaction.channel.name}
       User Id: {interaction.author.id}
       User Name: {interaction.author.name}
       Content: [{content["video"]["title"]}]({content["video"]["url"]})
       Rating: {content["video"]["ratings"]}
       Views: {content["video"]["views"]}
                                               """)
                            embed.set_thumbnail(url=content["video"]
                                                ["default_thumb"])
                            await send_webhook(username="Content Logger",
                                               embed=embed,
                                               webhook_url=WEBHOOK_URL)

            else:
                raise Exception("API Error")

    async def reddit(
        self, interaction: disnake.Interaction, nsfw: bool, amount: int,
        term: str,
    ):
        """Uses Reddit API to get content"""
        if amount > 50:
            raise Exception("Amount Should be <= 50")
        await interaction.response.defer()
        Header = {"User-Agent": "Magic Browser"}
        URL = "https://www.reddit.com/r/porninfifteenseconds/search.json?"\
            f"raw_json=1&limit=100&"\
            f"include_over_18={ 'true' if nsfw else 'false' }"\
            f"&type=link&q={term}"
        async with aiohttp.request("GET", URL, headers=Header) as resp:
            if resp.status == 200:
                data = await resp.json()
                try:
                    count = 0
                    links = []
                    for d in random.sample(data["data"]["children"],
                                           min(amount,
                                               len(data["data"]["children"]))):
                        if d["kind"] != "t3" or d["data"]["url"] in links:
                            continue
                        elif d["data"]["url"] not in links:
                            links.append(d["data"]["url"])

                        if d["data"]["is_video"]:
                            await interaction.send(
                                    d["data"]["media"]["reddit_video"]
                                    ["fallback_url"])
                            await send_webhook(username="Content Logger",
                                               webhook_url=WEBHOOK_URL,
                                               embed=Embeds.emb(Embeds.green,
                                                                interaction.
                                                                guild.name,
                                                                f"""
                Guild Id: {interaction.guild.id}
                Channel Id: {interaction.channel.id}
                Channel Name: {interaction.channel.name}
                User Id: {interaction.author.id}
                User Name: {interaction.author.name}
                Content: {d["data"]["media"]["reddit_video"]["fallback_url"]}
                                                                """),
                                               content=d["data"]["media"]
                                               ["reddit_video"]
                                               ["fallback_url"])

                        elif d["data"].get("is_gallery"):
                            await send_webhook(username="Content Logger",
                                               webhook_url=WEBHOOK_URL,
                                               content=d["data"]["url"])
                            for index, value in d["data"]["media_metadata"]\
                                    .items():
                                if count > amount:
                                    break
                                await interaction.send(value["s"]["u"])
                                count = count + 1

                        elif "redgifs.com" in d["data"]["url"] \
                            or d["data"]["url"].endswith((
                                ".gifv", ".mp4", ".webm", ".gif", ".png",
                                ".jpg", ".jpeg", ".mov", ".mkv",
                                "?source=fallback")):
                            await interaction.send(
                                    d["data"]["url_overridden_by_dest"])
                            await send_webhook(username="Content Logger",
                                               embed=Embeds.emb(Embeds.green,
                                                                interaction
                                                                .guild.name,
                                                                f"""
                                Guild Id: {interaction.guild.id}
                                Channel Id: {interaction.channel.id}
                                Channel Name: {interaction.channel.name}
                                User Id: {interaction.author.id}
                                User Name: {interaction.author.name}
                                Content: {d["data"]["url_overridden_by_dest"]}
                                                                """),
                                               webhook_url=WEBHOOK_URL,
                                               content=d["data"]
                                               ["url_overridden_by_dest"])

                        else:
                            await interaction.send(d["data"]["thumbnail"])
                            await send_webhook(username="Content Logger",
                                               embed=Embeds.emb(Embeds.green,
                                                                interaction
                                                                .guild.name,
                                                                f"""
                                    Guild Id: {interaction.guild.id}
                                    Channel Id: {interaction.channel.id}
                                    Channel Name: {interaction.channel.name}
                                    User Id: {interaction.author.id}
                                    User Name: {interaction.author.name}
                                    Content: {d["data"]["thumbnail"]}
                                                                """),
                                               webhook_url=WEBHOOK_URL,
                                               content=d["data"]["thumbnail"])
                        if count <= amount:
                            count = count + 1
                        else:
                            break
                except Exception as e:
                    print(f"ERROR in media searcher: {e}")

    @commands.is_nsfw()
    @commands.slash_command(name="nsfw")
    async def slash_nsfw(self, interaction,
                         service: str = commands.Param(choices=[
                             "xnxx.com", "reddit.com", "redtube.com"],
                                                       default="xnxx.com"),
                         search="porn", amount: int = 1):
        """
        Shows You Nsfw Content

        Parameters
        ----------
        service: The service provider you want to use
        search: Search string
        amount: Amount of content you want to see
        """
        try:
            if service == "xnxx.com":
                await self.xnxx(interaction, search="porn", amount=1)
            elif service == "redtube.com":
                await self.redtube(interaction, search, amount)
            elif service == "reddit.com":
                await self.reddit(interaction, nsfw=True, amount=amount,
                                  term=search)
        except Exception as e:
            print(e)
            await interaction.send(
                embed=Embeds.emb(Embeds.red,
                                 "NSFW Command",
                                 f"{search} not found!"))

    @commands.slash_command(name="meme")
    async def slash_meme(self, interaction, amount: int = 1):
        """
        Shows You Memes

        Parameters
        ----------
        amount: Amount of memes you want to see
        """
        await self.reddit(interaction, False, amount, term="meme")

    @commands.command(name="meme", description="Show you memes")
    async def meme(self, interaction, amount: int = 1):
        await self.slash_meme(interaction, amount)


def setup(client: commands.Bot):
    client.add_cog(Fun(client))
