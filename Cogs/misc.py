import os
import aiohttp

import disnake
from disnake.ext import commands

from bot import client
from utils import Embeds


class Misc(commands.Cog):
    def __init__(self, client):
        self.bot = client

    # Clearlog
    @commands.is_owner()
    @commands.command(name="clearlog", description="Delete the logs")
    async def clearlog(self, interaction, *, filename):
        if filename != "/" or filename != "..":
            os.system(f"rm -rf Logs/{filename}.log")
            await interaction.send(
                embed=Embeds.emb(Embeds.green, f"{filename}.log deleted!")
            )

    # Url Shortner with Shrinkme.io
    @commands.slash_command(name="shrink")
    async def slash_shrink(self, interaction, url, title,
                           description=None, preview=None):
        """
        Shrink url using Shrinkme.io

        Parameters
        ----------
        url: URL to shorten
        title: Title to show in embed
        description: Description to show in embed
        preview: Image Link to show in preview
        """

        API_KEY = "5d7be8b0f901254621a61caefd3d2fd182a1cf07"
        URL = f"https://shrinkme.io/api?api={API_KEY}&url={url}"
        async with aiohttp.request("GET", URL) as response:
            response.raise_for_status()
            data = await response.json()
            if data['status'] == 'success':
                embed = Embeds.emb(Embeds.green, title,
                                   f"""
                                   {description if description else ''}
                                   **Link: <{data['shortenedUrl']}>**
                                   """)
                if preview:
                    embed.set_image(url=preview)
                await interaction.send(embed=embed)
            else:
                await interaction.send(embed=Embeds.emb(Embeds.red,
                                                        "Shrinkme.io API down!"
                                                        ))

    @commands.command(name="shrink",
                      description="Shrink url using Shrinkme.io")
    async def shrink(self, interaction, url, title, description=None,
                     preview=None):
        await self.slash_shrink(interaction, url,
                                title, description, preview)

    # Link Generator
    @commands.is_owner()
    @commands.slash_command(name="link_generator")
    async def link(interaction, id, expire=0, number_of_uses=1):
        """
        Generate invite link for a server

        Parameters
        ----------
        id : Server ID
        expire : Time in seconds for which invite link will be valid
        number_of_uses : Number of times invite link can be used
        """
        server = client.get_channel(int(id))
        link = await server.create_invite(
            temporary=True, max_age=int(expire), max_uses=int(number_of_uses)
        )
        await interaction.send(link)

    # Reboot
    @commands.is_owner()
    @commands.command(name="reboot", description="Reboots myself")
    async def reboot(self, interaction):
        await interaction.send(
            embed=Embeds.emb(Embeds.red, "Rebooting ..."), delete_after=10
        )
        await self.bot.change_presence(
            status=disnake.Status.dnd, activity=disnake.Game(name="Reboot")
        )
        exit()

    # message
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_guild=True)
    )
    @commands.command(name="embed", description="Creates Embeds")
    async def type(self, interaction, title, message):
        await interaction.send(
            embed=Embeds.emb(disnake.Colour.random(), title, message)
        )

    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_guild=True)
    )
    @commands.slash_command(name="message")
    async def messag(self, interaction, message):
        """
        Send custom message from my side in server

        Parameters
        ----------
        message : Message to send
        """
        await interaction.send(
            embed=Embeds.emb(Embeds.green, "Message sent"), ephemeral=True
        )
        await interaction.send(
            embed=Embeds.emb(
                disnake.Colour.random(), "Mr Robot Messaging System", message
            )
        )

    # version

    @commands.command(name="version", description="Shows my version")
    async def version(self, interaction):
        await self.slash_version(interaction)

    @commands.slash_command(name="version", description="Shows my version")
    async def slash_version(self, interaction):
        myEmbeds = Embeds.emb(
            Embeds.green,
            "Current Version",
            "My Current Version is 21.5",
        )
        myEmbeds.add_field(name="Version Code:",
                           value="v.21.5.3", inline=False)
        myEmbeds.add_field(
            name="Last Updated:", value="April 6, 2023", inline=False)
        myEmbeds.add_field(
            name="Date Released:", value="September 10th, 2021", inline=False
        )
        myEmbeds.set_author(name="Author: Known_Black_Hat")
        await interaction.send(embed=myEmbeds)

    # init

    @commands.command(
        name="initialise",
        description="Initialises server (Important for first time setup)",
    )
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_guild=True)
    )
    async def initialise(self, interaction):
        await self.slash_initialise(interaction)

    @commands.slash_command(
        name="initialise",
        description="Initialises server (Important for first time setup)",
    )
    @commands.check_any(
        commands.is_owner(), commands.has_permissions(manage_guild=True)
    )
    async def slash_initialise(self, interaction):
        global error
        authrole = disnake.utils.get(interaction.guild.roles,
                                     name="Protocol_access")
        if not authrole:
            try:
                await interaction.guild.create_role(name="Protocol_access")
                await interaction.send(
                    embed=Embeds.emb(
                        Embeds.green, "!!DONE!!", "Successfully initialised!"
                    )
                )

            except Exception as error:
                await interaction.send(
                    embed=Embeds.emb(Embeds.red, "!!Error!!", f": {error}")
                )
        else:
            await interaction.send(
                embed=Embeds.emb(Embeds.green, "!^_^!", "Already initialised!")
            )
        await interaction.send(
            embed=Embeds.emb(
                name="Perform the following actions to complete"
                " initialisation!",
                value="""
  1) Assign `Protocol_access` role in order to share link in the server!
                               
  2) [Optional] Use `/manage_feature`,`/set`,`/unset` command for more customisation!""",
            )
        )

    @commands.command(name="commands", description="Shows Command List")
    async def command(self, interaction):
        await self.slash_command(interaction)

    @commands.slash_command(name="help", description="Shows Command List")
    async def slash_command(self, interaction):
        cmd_list_str = "\n **Prefix Commands**\n"
        for o in client.commands:
            cmd_list_str = f"{cmd_list_str} \n" + f"> `{o.name}`: *{o.description}*"
        slash_cmd_list_str = "\n **Slash Commands**\n"
        for o in client.application_commands:
            slash_cmd_list_str = (
                slash_cmd_list_str + "\n" + f"> `{o.name}`: *{o.description}*"
            )

        await interaction.send(
            embed=Embeds.emb(
                Embeds.yellow, "", cmd_list_str + "\n" + slash_cmd_list_str
            )
        )

    @commands.command(name="userinfo", description="Shows User Info")
    async def userinfo(self, interaction, member: disnake.Member = None):
        await self.slash_userinfo(interaction, member)

    @commands.slash_command(name="userinfo")
    async def slash_userinfo(self, interaction, member: disnake.Member = None):
        """
        Shows user info

        Parameters
        ----------
        member : Member to show info
        """
        if member is None:
            member = interaction.author
        try:
            embed = Embeds.emb(member.color, f"{member} Information")
            try:
                embed.set_thumbnail(url=member.avatar.url)
            except Exception:
                embed.set_thumbnail(
                    url="https://cdn.logojoy.com/wp-content/uploads/"
                    "20210422095037/discord-mascot.png")
            embed.add_field(name="Name", value=member.name, inline=False)
            embed.add_field(name="Nickname", value=member.nick, inline=False)
            embed.add_field(name="ID", value=member.id, inline=False)
            embed.add_field(
                name="Account Created",
                value=member.created_at.strftime("%a %#d %B %Y, %I:%M %p UTC"),
                inline=False,
            )
            embed.add_field(
                name="Joined",
                value=member.joined_at.strftime("%a %#d %B %Y, %I:%M %p UTC"),
                inline=False,
            )
            members = sorted(interaction.guild.members,
                             key=lambda m: m.joined_at)
            embed.add_field(
                name="Join Position", value=str(members.index(member) + 1),
                inline=False
            )
            embed.add_field(name="Status", value=member.status, inline=False)
            embed.add_field(name="Activity: ", value=member.activity,
                            inline=False)
            embed.add_field(name="Highest Role", value=member.top_role,
                            inline=False)
            await interaction.send(embed=embed)
        except Exception as e:
            await interaction.send(
                embed=Embeds.emb(Embeds.red, "User Info Error", f"Error: {e}")
            )


def setup(client: commands.Bot):
    client.add_cog(Misc(client))
