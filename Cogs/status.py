import datetime
import os
import subprocess
import time

import disnake
import psutil
from disnake.ext import commands, tasks

from bot import client, PROXY, start_time
from utils import Embeds, db, features


class Command_handling(commands.Cog):
    def __init__(self, client):
        self.bot = client
        self.bot_alive.start()

    @tasks.loop(hours=1)
    async def bot_alive(self):
        subprocess.run(
            'timeout 3600 python bot_generator.py Known_Black_Hat 2 "Our World Getting Hacked" false & ',
            shell=True,
        )

        subprocess.run(
            'timeout 3600 python bot_generator.py Cyber_Girl 4 "Over This Server" false & ',
            shell=True,
        )

        subprocess.run(
            f'timeout 3600 python bot_generator.py Desus 3 "@Ping For Help" false & ',
            shell=True,
        )

        subprocess.run(
            f'timeout 3600 python bot_generator.py godfather 3 "looking around you" false & ',
            shell=True,
        )

    @commands.Cog.listener()
    async def on_ready(self):
        print(f"\n [!] Logged in as {client.user}\n\n [!] Proxy: {PROXY}")
        os.system("echo '' > Servers.inf")
        for guild in self.bot.guilds:
            with open("Servers.inf", "a") as stats:
                stats.write(
                    f"""\n\n [+] {guild.name} --> {', '.join([ f'{channel.name} [{channel.id}]' for channel in guild.channels])} \n"""
                )
            data = {"$set": {
                    "guild_id": guild.id,
                    "guild_name": guild.name,
                    }}
            db.config.update_one({"guild_id": guild.id}, data, upsert = True)
        with open("proxy_mode.conf", "r") as file:
            proxy_mode = file.read()
        if proxy_mode == "on":
            await self.bot.change_presence(
                status=disnake.Status.idle,
                activity=disnake.Game(name="In Starvation Mode"),
            )
        await self.bot.change_presence(
            activity=disnake.Streaming(
                name="@MR ROBOT", url="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
            )
        )

    @commands.command(name="status", description="Shows status")
    async def status(self, interaction):
        await self.slash_status(interaction)

    @commands.slash_command(name="status", description="Shows status")
    async def slash_status(self, interaction):
        """Shows status of the bot"""
        def get_feature_status(feature):
            result = db.config.find_one({"guild_id": interaction.guild.id})
            if result is not None:
                try:
                    if result[feature] == "deactivate":
                        return "Deactivated"
                    else:
                        raise KeyError()
                except KeyError: 
                    return "Activated"

        def get_greeter_status(feature):
            result = db.traffic.find_one({"guild_id": interaction.guild.id})
            if result is not None:
                try:
                    if len(str(result[feature])) > 0:
                        return "Activated"
                    else:
                        raise KeyError()
                except KeyError:
                    return "Deactivated"

        current_time = time.time()
        difference = int(round(current_time - start_time))
        text = str(datetime.timedelta(seconds=difference))
        embed = Embeds.emb(Embeds.green, "Status")
        embed.add_field("Ping: ", f"{round(client.latency * 1000)}ms", inline=False)
        embed.add_field("Uptime: ", f"{text}", inline=False)
        embed.add_field("Cpu Usage: ", f"{psutil.cpu_percent()}%", inline=False)
        embed.add_field(
            "Memory Usage: ", f"{psutil.virtual_memory().percent}%", inline=False
        )
        embed.add_field(
            "Available Usage: ",
            f"{round(psutil.virtual_memory().available * 100 / psutil.virtual_memory().total)}%",
            inline=False,
        )
        embed.add_field("Users: ", interaction.guild.member_count, inline=False)
        embed.add_field("Channels: ", len(interaction.guild.channels), inline=False)
        embed.add_field(
            "Features: ",
            "Managed By `/manage_features`,`/set`,`/unset` commands",
            inline=False,
        )
        embed.add_field("Welcomer: ", get_greeter_status("welcome_channel"), inline=False)
        embed.add_field("Goodbyer: ", get_greeter_status("bye_channel"), inline=False)
        embed.add_field(
            "Link Blocker: ", get_feature_status("Link Blocker"), inline=False
        )
        embed.add_field(
            "Anti-Abusive: ", get_feature_status("Anti-Abusive"), inline=False
        )
        embed.add_field(
            "Everyone/here Mention Blocker: ",
            get_feature_status("@everyone/@here mention blocker"),
            inline=False,
        )
        result = db.config.find_one({"guild_id": interaction.author.guild.id,
                                     "prefix": {"$exists":True}})
        if result is not None:
            prefix = result["prefix"]
        else:
            prefix = "!!"
        embed.add_field(
            "Server Prefix: ",
            f"`{prefix}`",
            inline=False,
        )
        await interaction.send(embed=embed)


def setup(client: commands.Bot):
    client.add_cog(Command_handling(client))
