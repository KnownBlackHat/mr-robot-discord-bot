import os
import subprocess

import disnake
from disnake.ext import commands

from utils import Embeds

REPO_URL = "https://github.com/KnownBlackHat/Mr-Robot_discord-bot.git"


class Oscmd(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.is_owner()
    @commands.command(name="cmd", description="Runs Console Commands")
    async def cmd(self, ctx, *, command_string):
        output = subprocess.getoutput(command_string)
        await ctx.send(
            embed=Embeds.emb(
                Embeds.green, "Shell Console", f"```\n{output[:1900]}\n```"
            )
        )

    @commands.is_owner()
    @commands.command(name="update",
                      description="Updates the code from gitlab")
    async def update(self, ctx):
        await self.bot.change_presence(
            status=disnake.Status.dnd, activity=disnake.Game(name="Update")
        )
        await ctx.send(embed=Embeds.emb(Embeds.green, "Updating..."))
        os.system("rm -rf mr-robot_discord-bot")
        os.system(f"git clone {REPO_URL}")
        for i in os.listdir():
            if i == "mr-robot_discord-bot":
                ...
            elif i == ".env" or i == "Logs":
                ...
            else:
                subprocess.getoutput(f"rm -rf {i}")
        subprocess.getoutput("mv mr-robot_discord-bot/* .")
        await ctx.send(embed=Embeds.emb(Embeds.green, "Update Completed"))
        os.system("python main.py")


def setup(client: commands.Bot):
    client.add_cog(Oscmd(client))
