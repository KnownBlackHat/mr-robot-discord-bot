# MR ROBOT Discord Bot

MR ROBOT is an advanced, multitasking Discord bot coded in Python that you can use for free in your Discord server.

## Features:

- Welcome & Goodbye notifier
- Moderation
- Cool NSFW and Meme commands 
- Music Player
- Offensive Word Blocker
- @everyone & @here mention Blocker
- Link Blocker
- Google Translation
- Basic Misc 
- Create Custom Embed

## Installation:

1. Clone this repository
2. Install the required packages using `pip install -r requirements.txt`
3. Create a `.env` file and add your bot token to it as `Mr_Robot=<TOKEN HERE>` **(replace `TOKEN HERE` with your bot token)**
4. Run the bot using `python main.py` command

## Contributing:

If you have any suggestions or feature requests, please create a pull request or open an issue on Github. 

## License:

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
