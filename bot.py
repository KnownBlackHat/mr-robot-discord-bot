""" Mr Robot Bot  """

import datetime
import os
import time

import disnake
from disnake.ext import commands
from dotenv import load_dotenv

from utils import Embeds, proxy_generator, db
import logging


PROXY = proxy_generator()
with open("proxy_mode.conf", "r", encoding="utf-8") as file:
    data = file.read()
if data != "on":
    PROXY = None
start_time = time.time()


def get_prefix(client, message):
    """ Gets Server Prefix if available """
    result = db.config.find_one({"guild_id": message.guild.id,
                                 "prefix": {"$exists": True}})
    if result is not None:
        prefix = result["prefix"]
    else:
        prefix = "!!"
    return commands.when_mentioned_or(prefix)(client, message)


class MrRobot(commands.Bot):
    """ Mr Robot Bot """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.start_time = time.time()
        self.logger = logging.getLogger("MrRobot")
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter(
                '%(asctime)s %(filename)s:%(lineno)d %(funcName)s'
                ' %(levelname)s: %(message)s')
        file_handler = logging.FileHandler("MrRobot.log")
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)
        self.logger.info("Mr Robot Bot Started")


client = MrRobot(
    proxy=PROXY, command_prefix=get_prefix, intents=disnake.Intents.all()
)
load_dotenv()


unloaded_cog_list = []
loaded_cog_list = []


try:
    for file in os.listdir("Cogs"):
        if file.endswith(".py"):
            try:
                client.load_extension(f"Cogs.{str(file[:-3])}")
                loaded_cog_list.append(file[:-3])
            except Exception as e:
                print(e)
                unloaded_cog_list.append(file[:-3])

except Exception as error:
    with open("Logs/error.log", "a", encoding="utf-8") as file:
        file.write(f"\nCogs Error: {error}")


@client.remove_command("help")
@commands.is_owner()
@client.command(description="Shows all loaded Cogs")
async def list_functions(ctx):
    """ Shows all loaded Cogs """
    await ctx.send(
        embed=Embeds.emb(
            Embeds.green,
            "Loaded Cogs",
            '\n\n✅ -> ' + '\n\n✅ -> '.join(loaded_cog_list)
            )
    )
    if unloaded_cog_list:
        await ctx.send(
            embed=Embeds.emb(
                Embeds.red,
                "Unloaded Cogs",
                '\n\n❌ -> ' + '\n\n❌ -> '.join(unloaded_cog_list)
            )
        )


@commands.is_owner()
@client.command(description="Load Cogs")
async def load(ctx, name: str = commands.Param(choices=unloaded_cog_list)):
    """ Loads Cogs """
    client.load_extension(f"Cogs.{name}")
    unloaded_cog_list.remove(name)
    loaded_cog_list.append(name)

    await ctx.send(embed=Embeds.emb(
        Embeds.green,
        "Loaded",
        f"{name} function"
        ))


@commands.is_owner()
@client.command(description="Reload Cogs")
async def reload(
        ctx: disnake.ApplicationCommandInteraction,
        name: str = commands.Param(choices=loaded_cog_list),
        ):
    """ Reloads Cogs """
    client.unload_extension(f"Cogs.{name}")
    loaded_cog_list.remove(name)
    unloaded_cog_list.append(name)
    client.load_extension(f"Cogs.{name}")
    unloaded_cog_list.remove(name)
    loaded_cog_list.append(name)
    await ctx.send(embed=Embeds.emb(
        Embeds.green, "Reloaded",
        f"{name} function"
        ))


@commands.is_owner()
@client.command(description="Unloads Cogs")
async def unload(ctx, name: str = commands.Param(choices=loaded_cog_list)):
    """ Unloads Cogs """
    client.unload_extension(f"Cogs.{name}")
    loaded_cog_list.remove(name)
    unloaded_cog_list.append(name)
    await ctx.send(embed=Embeds.emb(
        Embeds.red, "Unloaded",
        f"{name} function"
        ))

try:
    client.loop.run_until_complete(client.start(os.getenv("Mr_Robot")))
except Exception as e:
    print(e)
    if "429" in str(e):
        print("\n [+] Turning on Proxy Mode")
        with open("proxy_mode.conf", "w", encoding="utf-8") as file:
            file.write("on")
            print(f"\n [!] Login Failure at {datetime.datetime.now()}")
            print(f"\n [!] Proxy: {PROXY}")

finally:
    client.loop.close()
